import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Random;

/**
 * @Author: Jiax
 * @Description:
 * @Date: Created in 2019/4/3
 * @Modified By:
 */
public class CrawTextThread extends Thread {

    private static String localDir = "D:\\Spider\\";

    private List<String> list;


    /**
     * 创建文件
     *
     * @param fileName
     * @return
     */
    public static void createFile(File fileName) throws Exception {
        try {
            if (!fileName.exists()) {
                fileName.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 写入文本
     *
     * @param fileName 文件类
     * @param content  要写入的文本
     */
    public static void writeTxtFile(File fileName, String content) {
        FileOutputStream o = null;
        try {
            o = new FileOutputStream(fileName);
            o.write(content.getBytes("UTF-8"));
            o.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public CrawTextThread(List<String> list) {
        this.list = list;
    }


    @Override
    public void run() {
        currentThread().setName("线程:"+new Random().toString());
        for (String url : list) {
            try {
                System.out.println(("url：" + url));
                Document document = Jsoup.connect(url).timeout(6000).get();
                String title = document.getElementsByClass("j_chapterName").text();
                String content = document.getElementsByClass("read-content j_readContent").text();

                File file = new File(localDir + title + ".txt");
                createFile(file);
                System.out.println(("本地目录：" + localDir + title));
                writeTxtFile(file, FileterHtml(content));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static String FileterHtml(String str) {
        return str.replaceAll(" ", "").replaceAll("<br>", "\r\n");

    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

}
