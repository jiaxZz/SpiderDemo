import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Jiax
 * @Description:
 * @Date: Created in 2019/3/28
 * @Modified By:
 */
public class Spider {

    /**
     * @param url 目标地址
     */
    public static void getAllLinks(String url) {
        Document doc = null;
        try {
            doc = Jsoup.parse(HttpUtils.get(url));
        } catch (Exception e) {
            //接收到错误链接（404页面;
            return;
        }
        List<String> urlList = new ArrayList<String>();
        //Elements urlNodes = doc.getElementsByClass("cf").select("a");
        Elements urlNodes = doc.select("ul.cf li a");
        //同时抓取该页面图片链接
        for (Element urlNode : urlNodes) {
            urlList.add("https:"+urlNode.attr("href"));
        }
        CrawTextThread crawTextThread = new CrawTextThread(urlList);
        crawTextThread.start();
    }
}


